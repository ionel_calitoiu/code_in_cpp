#include "A.h"
#include <iostream>

A::A(int size):data{new int[size]}{
	std::cout << "class A size constructor called" << std::endl;
}
A::A():data(new int[1]),size{1}{
	std::cout << "class A default constructor called" << std::endl;
}
A::~A(){
	delete[] data;
	std::cout << "class A destructor called" << std::endl;
}
int& A::operator()(int index){
	return data[index];
}
