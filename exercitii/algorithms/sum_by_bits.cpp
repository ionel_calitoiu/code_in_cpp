#include <iostream>
#include <stdio.h>
#include "A.h"

void sum(int* a,int* b,int* rez,int n){
	int carry;
	for(int i = n-1;i >= 0;--i){
		rez[i] = a[i] & b[i];

		rez[i] = rez[i] | ((a[i] ^ b[i]) & rez[i+1]);
		rez[i+1] = rez[i+1] | (a[i] ^ b[i]);
	}
}

void print(int* a,int n){
	for(int i = 0;i < n;++i)
		std::cout << a[i] << " ";
	std::cout << std::endl;
}

int main(int argc,char** argv){
	int n = 5;
	int a[] = {1,1,1,1,0};
	int b[] = {1,1,1,0,1};
	int c[] = {0,0,0,0,0,0};
	sum(a,b,c,n);
	print(c,n+1);

	return 0;
}
