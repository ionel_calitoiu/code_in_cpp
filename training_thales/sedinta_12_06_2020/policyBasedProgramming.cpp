#include <iostream>
using namespace std;

class A
{
public:
	A(int v=0): val(v) { cout << "Ctor " << val << endl; }
	//inline ~A() { cout << "Dtor " << val << endl; }
	int val;
};

template<typename T, int size>
class SP
{
	T *p;
public:
	SP(T* p = nullptr): p(p) {}
	~SP() {
		delete[] p;
	}
	T* operator ->() { return p; }
	T& operator *() { return *p; }
};
template<typename T>
class SP <T, 0>
{
	T* p;
public:
	SP(T* p = nullptr) : p(p) {}
	~SP() {
		delete p;
	}
	T* operator ->() { return p; }
	T& operator *() { return *p; }
};


void f() {
	SP<A> sp(new A);
	SP<A, 10> sp2(new A[4]);

	cout << sp[1].val << endl;
}
// type traits
int main()
{
	A* p = new A[1];  // new A
	delete[] p;

	//f();
}
