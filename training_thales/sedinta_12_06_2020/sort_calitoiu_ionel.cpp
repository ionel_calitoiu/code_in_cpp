#include <iostream>
#include <string>
#include <list>
#include <algorithm>    // std::sort


struct Person{
	std::string firstName, lastName;
	int age;
	Person(std::string f,std::string l, int a): firstName(f),lastName(l),age(a){}
	friend std::ostream& operator << (std::ostream& os, const Person&);
};

void print(std::list<Person> const &list)
{
	for (auto const& i: list) {
		std::cout << i << "\n";
	}
}

bool compare_persons_by_age (const Person& a,const Person& b){
	return a.age < b.age;
}

enum Order_Nume_Prenume{
	NUME_PRENUME = 1,
	PRENUME_NUME,
	PRENUME,
	NUME
};

bool compare_person_by_last_first(const Person& a,const Person& b){
	if(a.lastName != b.lastName)
		return a.lastName < b.lastName;
	return a.firstName < b.firstName;
}

bool compare_person_by_first_last(const Person& a,const Person& b){
	if(a.firstName != b.firstName)
		return a.firstName < b.firstName;
	return a.lastName < b.lastName;
}

bool compare_person_by_first(const Person& a,const Person& b){
	return a.firstName < b.firstName;
}

bool compare_person_by_last(const Person& a,const Person& b){
	return a.lastName < b.lastName;
}

std::ostream& operator << (std::ostream& os, const Person& p){
	    os << p.firstName  << ' ' << p.lastName << ' ' << p.age;
    	return os;
	}

void reportByAge(std::list<Person>& list){
	list.sort(compare_persons_by_age);
}

void reportByName(std::list<Person>& list,Order_Nume_Prenume criteriu){
	switch(criteriu){
		case NUME_PRENUME:
			list.sort(compare_person_by_last_first);
			break;
		case PRENUME_NUME:
			list.sort(compare_person_by_first_last);
			break;
		case PRENUME:
			list.sort(compare_person_by_first);
			break;
		case NUME:
			list.sort(compare_person_by_last);
			break;
	}
}
void report(std::list<Person>& list,const std::list<std::string>& criterii){
	list.sort(compare_persons_by_age,compare_person_by_first);
}

int main(int argc,char** argv){

	std::list<Person> persons{
		Person("Ion","Adamescu",22),
		Person("Maria","Popescu",12),
		Person("Ana","Adamescu",22),
		Person("Viorel","Negru",7),
		Person("Radu","Vasile",10)
	};

	//reportByAge(persons);
	reportByName(persons,NUME_PRENUME);
	print(persons);
	report(persons,std::list<std::string>{"age","firstName"});
	
	return 0;
}