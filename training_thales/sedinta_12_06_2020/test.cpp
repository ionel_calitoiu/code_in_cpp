#include <iostream>

void funcc( (*func) (int)){
	func(5);
}

int main(int argc,char** argv){

	int a = 10;

	([&](int b){
		a = 11;
		std::cout << a;
	});
	std::cout << a;
	return 0;
}