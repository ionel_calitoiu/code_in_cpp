//exercitiu suprascriere new/delete
//Calitoiu Ionel
#include <iostream>

class ObjectPool;

class Event{
	public:
	int temperature;
	static ObjectPool objectPool;
	Event(int t) : temperature(t) {}
	Event(){}
	void *operator new(size_t);
	void operator delete(void*);
};

class ObjectPool
{
	Event events[20];
	int size;
	bool is_object_taken[20];

	public:
		ObjectPool() {}
		Event* getObject();
		void returnObject(Event*);
};

void* Event::operator new(size_t size) { return objectPool.getObject(); }
void Event::operator delete(void *p){ objectPool.returnObject((Event*)p);}

Event* ObjectPool::getObject(){
	
	if (size == 0)
	{
		*(is_object_taken) = true;
		size++;
		return (events);
	}
	for (int i = 0; i < size; ++i)
	{
		if (*(is_object_taken + i) == false)
		{
			*(is_object_taken + i) = true;
			return (events + i);
		}
	}
	return nullptr;
}
void ObjectPool::returnObject(Event *obj)
{
	for (int i = 0; i < size; ++i)
	{
		if ((events + i) == obj)
		{
			*(is_object_taken + i) = false;
		}
	}
}

int main(int argc, char **argv)
{
	Event *p = new Event(22);
	//Event *q = new Event(-1);
	std::cout << p->temperature;
	//std::cout << q->temperature;
	//delete q;
	delete p;
	return 0;
}