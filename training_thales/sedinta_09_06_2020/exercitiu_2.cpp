//exercitiu suprascriere '->'
//Calitoiu Ionel
#include <iostream>

struct A{
	void work(){std::cout << "WORK";}
	~A(){std::cout << "obj este distrus";}
};

class SP{
	A* p;
	public:
		SP(A* p):p(p){};
		A* operator->(){return p;}
		~SP(){delete p;}
};

void f(){
	SP sp(new A);
	sp->work();
	(sp.operator->())->work();
};

int main(int argc, char **argv)
{
	f();
	return 0;
}