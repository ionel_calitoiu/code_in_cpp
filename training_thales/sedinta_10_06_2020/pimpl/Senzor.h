#include <memory>

class SenzorImplementation;

class Senzor{
	std::unique_ptr<SenzorImplementation> pImplementation;
public:

	Senzor();
	void start();
	int getTemperature();
	~Senzor();
};