#include "Senzor.h"
#include "SenzorImplementation.h"

void Senzor::start(){
	pImplementation->start();

}

int Senzor::getTemperature(){

	return pImplementation->getTemperature();
}

Senzor::Senzor():pImplementation(new SenzorImplementation){
}
Senzor::~Senzor(){
}