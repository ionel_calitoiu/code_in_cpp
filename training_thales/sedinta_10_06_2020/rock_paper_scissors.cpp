#include <iostream>
//rock_paper_scissors

class Rock;
class Scissors;
class Paper;

class Option{
	public:

	virtual int getValue(){return 0;}
};

class Rock:public Option{
	public:
		virtual int getValue(){return 0;};
};

class Paper:public Option{
	public:
		virtual int getValue(){return 1;}
};

class Scissors:public Option{
	public:
		virtual int getValue(){return 2;}
};

int play(Option& player1,Option& player2){
	return (player1.getValue() - player2.getValue());
}

int main(int argc,char** argv){

	Paper p;
	Rock r;
	Scissors s;
	std::cout << play(r,s);
	return 0;
}