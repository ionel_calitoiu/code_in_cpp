#include <iostream>

class A{
public:
	int val;
	A(int v = 0):val(v){std::cout << "Ctor" << val << std::endl;}
	inline ~A(){std::cout << "Dtor " << val << std::endl;}
};

template<typename T>
class SP{
	T* p;
	public:
	SP(T* p = nullptr):p(p){}
	~SP(){
		delete p;
	}

	T* operator-> (){return p;}
	T& operator* (){return *p;}
};

template<typename T,int size>
class SP<T[size]>{
	T* p;
	public:
	SP(T* p = nullptr):p(p){}
	~SP(){
		delete[] p;
	}
	T& operator[] (int i){
		return p[i];
	}

	T* operator-> (){return p;}
	T& operator* (){return *p;}
};

void f(){
	SP<A[4]> sp(new A[4]);
	SP<A> sp2(new A);
	std::cout << (sp[2]).val << std::endl;
	std::cout << (*sp2).val << std::endl;
}

int main(int argc,char** argv){

	f();

	return 0;
}